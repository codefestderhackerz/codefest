package sample;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;


public class Report {

    void generateReportHour(ArrayList<ResultSet> x) throws Exception{

        PDDocument document = new PDDocument();

        for (ResultSet r : x) {


            DefaultCategoryDataset lineDataset = new DefaultCategoryDataset();
            DefaultCategoryDataset lineDataset2 = new DefaultCategoryDataset();
            while (r.next()) {
                lineDataset.addValue((Integer.parseInt(r.getString("Error"))*100/Integer.parseInt(r.getString("Total"))), "Errors", r.getString("Date")+"-"+r.getString("Month")+"-"+r.getString("Year")+" "+r.getString("Hour"));
                lineDataset2.addValue((Integer.parseInt(r.getString("Total"))), "Total", r.getString("Date")+"-"+r.getString("Month")+"-"+r.getString("Year"));
            }

            JFreeChart chart2 = ChartFactory.createLineChart("Total for Stage", "Year", "Total", lineDataset2);
            JFreeChart chart = ChartFactory.createLineChart("Errors Percentage for Stage", "Year", "Errors", lineDataset);

            try {
                ChartUtilities.saveChartAsJPEG(new File("/home/madnisal/chart.jpg"), chart, 500, 300);
                ChartUtilities.saveChartAsJPEG(new File("/home/madnisal/chart2.jpg"), chart2, 500, 300);
            } catch (Exception e) {
                System.out.println(e);
            }

            try {
                PDPage page = new PDPage(PDPage.PAGE_SIZE_A4);
                InputStream in = new FileInputStream(new File("/home/madnisal/chart.jpg"));
                document.addPage(page);
                PDJpeg img = new PDJpeg(document, in);
                PDPageContentStream contentStream = new PDPageContentStream(document, page);
                contentStream.drawImage(img, 10, 300);
                contentStream.close();

                page = new PDPage(PDPage.PAGE_SIZE_A4);
                in = new FileInputStream(new File("/home/madnisal/chart2.jpg"));
                document.addPage(page);
                img = new PDJpeg(document, in);
                contentStream = new PDPageContentStream(document, page);
                contentStream.drawImage(img, 10, 300);
                contentStream.close();


            } catch (IOException e) {
                System.out.println(e);
            }
        }
        document.save("/home/madnisal/save.pdf");
        document.close();


    }
    void generateReportDay(ArrayList<ResultSet> x) throws Exception{

        PDDocument document = new PDDocument();

        for (ResultSet r : x) {


            DefaultCategoryDataset lineDataset = new DefaultCategoryDataset();
            DefaultCategoryDataset lineDataset2 = new DefaultCategoryDataset();
            while (r.next()) {
                lineDataset.addValue((Integer.parseInt(r.getString("Error"))*100/Integer.parseInt(r.getString("Total"))), "Errors", r.getString("Date")+"-"+r.getString("Month")+"-"+r.getString("Year"));
                lineDataset2.addValue((Integer.parseInt(r.getString("Total"))), "Total", r.getString("Date")+"-"+r.getString("Month")+"-"+r.getString("Year"));
            }

            JFreeChart chart2 = ChartFactory.createLineChart("Total for Stage", "Year", "Total", lineDataset2);
            JFreeChart chart = ChartFactory.createLineChart("Errors Percentage for Stage", "Year", "Errors", lineDataset);

            try {
                ChartUtilities.saveChartAsJPEG(new File("/home/madnisal/chart.jpg"), chart, 500, 300);
                ChartUtilities.saveChartAsJPEG(new File("/home/madnisal/chart2.jpg"), chart2, 500, 300);
            } catch (Exception e) {
                System.out.println(e);
            }

            try {
                PDPage page = new PDPage(PDPage.PAGE_SIZE_A4);
                InputStream in = new FileInputStream(new File("/home/madnisal/chart.jpg"));
                document.addPage(page);
                PDJpeg img = new PDJpeg(document, in);
                PDPageContentStream contentStream = new PDPageContentStream(document, page);
                contentStream.drawImage(img, 10, 300);
                contentStream.close();

                page = new PDPage(PDPage.PAGE_SIZE_A4);
                in = new FileInputStream(new File("/home/madnisal/chart2.jpg"));
                document.addPage(page);
                img = new PDJpeg(document, in);
                contentStream = new PDPageContentStream(document, page);
                contentStream.drawImage(img, 10, 300);
                contentStream.close();


            } catch (IOException e) {
                System.out.println(e);
            }
        }
        document.save("/home/madnisal/save.pdf");
        document.close();


    }

    void generateReportMonth(ArrayList<ResultSet> x) throws Exception{

        PDDocument document = new PDDocument();

        for (ResultSet r : x) {


            DefaultCategoryDataset lineDataset = new DefaultCategoryDataset();
            DefaultCategoryDataset lineDataset2 = new DefaultCategoryDataset();
            while (r.next()) {
                lineDataset.addValue((Integer.parseInt(r.getString("Error"))*100/Integer.parseInt(r.getString("Total"))), "Errors", r.getString("Month")+"-"+r.getString("Year"));
                lineDataset2.addValue((Integer.parseInt(r.getString("Total"))), "Total", r.getString("Month")+"-"+r.getString("Year"));
            }

            JFreeChart chart2 = ChartFactory.createLineChart("Total for Stage", "Year", "Total", lineDataset2);
            JFreeChart chart = ChartFactory.createLineChart("Errors Percentage for Stage", "Year", "Errors", lineDataset);

            try {
                ChartUtilities.saveChartAsJPEG(new File("/home/madnisal/chart.jpg"), chart, 500, 300);
                ChartUtilities.saveChartAsJPEG(new File("/home/madnisal/chart2.jpg"), chart2, 500, 300);
            } catch (Exception e) {
                System.out.println(e);
            }

            try {
                PDPage page = new PDPage(PDPage.PAGE_SIZE_A4);
                InputStream in = new FileInputStream(new File("/home/madnisal/chart.jpg"));
                document.addPage(page);
                PDJpeg img = new PDJpeg(document, in);
                PDPageContentStream contentStream = new PDPageContentStream(document, page);
                contentStream.drawImage(img, 10, 300);
                contentStream.close();

                page = new PDPage(PDPage.PAGE_SIZE_A4);
                in = new FileInputStream(new File("/home/madnisal/chart2.jpg"));
                document.addPage(page);
                img = new PDJpeg(document, in);
                contentStream = new PDPageContentStream(document, page);
                contentStream.drawImage(img, 10, 300);
                contentStream.close();


            } catch (IOException e) {
                System.out.println(e);
            }
        }
        document.save("/home/madnisal/save.pdf");
        document.close();


    }

    void generateReportYear(ArrayList<ResultSet> x) throws Exception{

        PDDocument document = new PDDocument();

        for (ResultSet r : x) {


            DefaultCategoryDataset lineDataset = new DefaultCategoryDataset();
            DefaultCategoryDataset lineDataset2 = new DefaultCategoryDataset();
            while (r.next()) {
                lineDataset.addValue((Integer.parseInt(r.getString("Error"))*100/Integer.parseInt(r.getString("Total"))), "Errors", r.getString("Year"));
                lineDataset2.addValue((Integer.parseInt(r.getString("Total"))), "Total", r.getString("Year"));
            }

            JFreeChart chart2 = ChartFactory.createLineChart("Total for Stage", "Year", "Total", lineDataset2);
            JFreeChart chart = ChartFactory.createLineChart("Errors Percentage for Stage", "Year", "Errors", lineDataset);

            try {
                ChartUtilities.saveChartAsJPEG(new File("/home/madnisal/chart.jpg"), chart, 500, 300);
                ChartUtilities.saveChartAsJPEG(new File("/home/madnisal/chart2.jpg"), chart2, 500, 300);
            } catch (Exception e) {
                System.out.println(e);
            }

            try {
                PDPage page = new PDPage(PDPage.PAGE_SIZE_A4);
                InputStream in = new FileInputStream(new File("/home/madnisal/chart.jpg"));
                document.addPage(page);
                PDJpeg img = new PDJpeg(document, in);
                PDPageContentStream contentStream = new PDPageContentStream(document, page);
                contentStream.drawImage(img, 10, 300);
                contentStream.close();

                page = new PDPage(PDPage.PAGE_SIZE_A4);
                in = new FileInputStream(new File("/home/madnisal/chart2.jpg"));
                document.addPage(page);
                img = new PDJpeg(document, in);
                contentStream = new PDPageContentStream(document, page);
                contentStream.drawImage(img, 10, 300);
                contentStream.close();


            } catch (IOException e) {
                System.out.println(e);
            }
        }
        document.save("/home/madnisal/save.pdf");
        document.close();


    }

}