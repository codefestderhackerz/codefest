package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.event.ActionEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.event.*;

import javafx.scene.control.Button;


public class Main extends Application {

    private Controller c = new Controller();
    public static String current_process_name = "p1";
    public static int Timerx = 0;
    public static boolean buttonsadded = false;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/sample.fxml"));
        primaryStage.setTitle("Production Line");






        NumberAxis x = new NumberAxis();
        NumberAxis y = new NumberAxis();

        NumberAxis x1 = new NumberAxis();
        NumberAxis y1 = new NumberAxis();

        x.setLabel("Time");
        x.setForceZeroInRange(false);
        y.setLabel("Errors");
        x1.setLabel("Time");
        x1.setForceZeroInRange(false);
        y1.setLabel("Total");

        LineChart<Number, Number> LineChart = new LineChart<Number, Number>(x,y);
        LineChart<Number, Number> LineChart2 = new LineChart<Number, Number>(x1,y1);



        final XYChart.Series series = new XYChart.Series();
        final XYChart.Series series1 = new XYChart.Series();
        final XYChart.Series series2 = new XYChart.Series();
        final XYChart.Series series3 = new XYChart.Series();

        final XYChart.Series tseries = new XYChart.Series();
        final XYChart.Series tseries1 = new XYChart.Series();
        final XYChart.Series tseries2 = new XYChart.Series();
        final XYChart.Series tseries3 = new XYChart.Series();

        series.setName("Stage 1");
        series1.setName("Stage 2");
        series2.setName("Stage 3");
        series3.setName("Stage 4");

        tseries.setName("Stage 1");
        tseries1.setName("Stage 2");
        tseries2.setName("Stage 3");
        tseries3.setName("Stage 4");

        final FlowPane root1 = new FlowPane();



        for(String d : ServerSide.LineDetails.keySet()){
            buttonsadded = true;
            final Button myButton = new Button(d);
            myButton.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent event) {
                    Timerx = 0;
                    current_process_name = myButton.getText();
                    series.getData().clear();
                    series1.getData().clear();
                    series2.getData().clear();
                    series3.getData().clear();
                    tseries.getData().clear();
                    tseries1.getData().clear();
                    tseries2.getData().clear();
                    tseries3.getData().clear();
                }
            });
            root1.getChildren().add(myButton);
        }

        final Button viewEmployeeData = new Button("Get Employee Data");
        final Label ErrorLabel1D = new Label("Error Percentage for label 1");
        final Label CurrentProcessRate1D = new Label("Current Processing for label 1");
        final Label ErrorLabel2D = new Label("Error Percentage for label 2");
        final Label CurrentProcessRate2D = new Label("Current Processing for label 2");
        final Label ErrorLabel3D = new Label("Error Percentage for label 3");
        final Label CurrentProcessRate3D = new Label("Current Processing for label 3");
        final Label ErrorLabel4D = new Label("Error Percentage for label 4");
        final Label CurrentProcessRate4D = new Label("Current Processing for label 4");

        final Label ErrorLabel1 = new Label("");
        final Label CurrentProcessRate1 = new Label("");
        final Label ErrorLabel2 = new Label("");
        final Label CurrentProcessRate2 = new Label("");
        final Label ErrorLabel3 = new Label("");
        final Label CurrentProcessRate3 = new Label("");
        final Label ErrorLabel4 = new Label("");
        final Label CurrentProcessRate4 = new Label("");

        viewEmployeeData.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent event){
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CSVLayout.fxml"));
                    Parent roots = (Parent) fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(roots));
                    stage.show();
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }

        });
        root1.getChildren().add(viewEmployeeData);

        final Button getReportYear=new Button("Print Yearly Report");
        getReportYear.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("d");
                DatabaseConnection data = new DatabaseConnection();
                data.startConnection();
                Report r = new Report();
                try{
                    r.generateReportYear(data.GetDataYear());
                }catch(Exception e){
                    System.out.println(e);
                }
            }
        });

        final Button getReportMonthly=new Button("Print Monthly Report ");
        getReportMonthly.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("d");
                DatabaseConnection data = new DatabaseConnection();
                data.startConnection();
                Report r = new Report();
                try{
                    r.generateReportMonth(data.GetDataMonth());
                }catch(Exception e){
                    System.out.println(e);
                }
            }
        });

        final Button getReportDaily=new Button("Print Daily Report");
        getReportDaily.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("d");
                DatabaseConnection data = new DatabaseConnection();
                data.startConnection();
                Report r = new Report();
                try{
                    r.generateReportDay(data.GetDataDay());
                }catch(Exception e){
                    System.out.println(e);
                }
            }
        });

        final Button getReportHourly=new Button("Print Hourly Report");
        getReportHourly.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("d");
                DatabaseConnection data = new DatabaseConnection();
                data.startConnection();
                Report r = new Report();
                try{
                    r.generateReportHour(data.GetDataHour());
                }catch(Exception e){
                    System.out.println(e);
                }
            }
        });

        GridPane dataview = new GridPane();
        dataview.add(ErrorLabel1D,0,0);
        dataview.add(ErrorLabel1,0,1);
        dataview.add(CurrentProcessRate1D,0,2);
        dataview.add(CurrentProcessRate1,0,3);

        dataview.add(ErrorLabel2D,1,0);
        dataview.add(ErrorLabel2,1,1);
        dataview.add(CurrentProcessRate2D,1,2);
        dataview.add(CurrentProcessRate2,1,3);

        dataview.add(ErrorLabel3D,2,0);
        dataview.add(ErrorLabel3,2,1);
        dataview.add(CurrentProcessRate3D,2,2);
        dataview.add(CurrentProcessRate3,2,3);

        dataview.add(ErrorLabel4D,3,0);
        dataview.add(ErrorLabel4,3,1);
        dataview.add(CurrentProcessRate4D,3,2);
        dataview.add(CurrentProcessRate4,3,3);

        dataview.add(getReportHourly,4,0);
        dataview.add(getReportDaily,4,1);
        dataview.add(getReportMonthly,4,2);
        dataview.add(getReportYear,4,3);


        class updatethread extends Thread{
            class updaterunnable implements Runnable{
                int Timer;
                public updaterunnable(int Timer){
                    this.Timer = Timer;
                }
                public void run(){
                    if(series.getData().size()==3){
                        series.getData().remove(0);
                        series1.getData().remove(0);
                        series2.getData().remove(0);
                        series3.getData().remove(0);
                        tseries.getData().remove(0);
                        tseries1.getData().remove(0);
                        tseries2.getData().remove(0);
                        tseries3.getData().remove(0);
                    }
                    series.getData().add(new XYChart.Data((Number)Timer,(Number)(100*ServerSide.LineDetails.get(current_process_name)[0][0]/ServerSide.LineDetails.get(current_process_name)[0][1])));
                    series1.getData().add(new XYChart.Data((Number)Timer,(Number)(100*ServerSide.LineDetails.get(current_process_name)[1][0]/ServerSide.LineDetails.get(current_process_name)[1][1])));
                    series2.getData().add(new XYChart.Data((Number)Timer,(Number)(100*ServerSide.LineDetails.get(current_process_name)[2][0]/ServerSide.LineDetails.get(current_process_name)[2][1])));
                    series3.getData().add(new XYChart.Data((Number)Timer,(Number)(100*ServerSide.LineDetails.get(current_process_name)[3][0]/ServerSide.LineDetails.get(current_process_name)[3][1])));
                    tseries.getData().add(new XYChart.Data((Number)Timer,(Number)(ServerSide.LineDetails.get(current_process_name)[0][1]/10)));
                    tseries1.getData().add(new XYChart.Data((Number)Timer,(Number)(ServerSide.LineDetails.get(current_process_name)[1][1]/10)));
                    tseries2.getData().add(new XYChart.Data((Number)Timer,(Number)(ServerSide.LineDetails.get(current_process_name)[2][1]/10)));
                    tseries3.getData().add(new XYChart.Data((Number)Timer,(Number)(ServerSide.LineDetails.get(current_process_name)[3][1]/10)));
                    CurrentProcessRate1.setText(""+ServerSide.LineDetails.get(current_process_name)[0][1]/10+" items per second");
                    CurrentProcessRate2.setText(""+ServerSide.LineDetails.get(current_process_name)[1][1]/10+" items per second");
                    CurrentProcessRate3.setText(""+ServerSide.LineDetails.get(current_process_name)[2][1]/10+" items per second");
                    CurrentProcessRate4.setText(""+ServerSide.LineDetails.get(current_process_name)[3][1]/10+" items per second");
                    ErrorLabel1.setText(""+100*ServerSide.LineDetails.get(current_process_name)[0][0]/ServerSide.LineDetails.get(current_process_name)[0][1]+"%");
                    ErrorLabel2.setText(""+100*ServerSide.LineDetails.get(current_process_name)[1][0]/ServerSide.LineDetails.get(current_process_name)[1][1]+"%");
                    ErrorLabel3.setText(""+100*ServerSide.LineDetails.get(current_process_name)[2][0]/ServerSide.LineDetails.get(current_process_name)[2][1]+"%");
                    ErrorLabel4.setText(""+100*ServerSide.LineDetails.get(current_process_name)[3][0]/ServerSide.LineDetails.get(current_process_name)[3][1]+"%");
                    int[][] x = {{0,0},{0,0},{0,0},{0,0}};
                    synchronized (new Object()) {
                        ServerSide.LineDetails.put("p1", x);
                    }
                }
            }
            public void run(){
                Timerx = 0;
                if(!buttonsadded){
                    for(String d : ServerSide.LineDetails.keySet()){
                        buttonsadded = true;
                        final Button myButton = new Button(d);
                        myButton.setOnAction(new EventHandler<ActionEvent>() {
                            public void handle(ActionEvent event) {
                                Timerx = 0;
                                current_process_name = myButton.getText();
                                series.getData().clear();
                                series1.getData().clear();
                                series2.getData().clear();
                                series3.getData().clear();
                                tseries.getData().clear();
                                tseries1.getData().clear();
                                tseries2.getData().clear();
                                tseries3.getData().clear();
                            }
                        });
                        root1.getChildren().add(myButton);
                    }
                }
                try{
                    while(true){
                        Thread.sleep(10000);
                        Timerx = Timerx + 10;
                        Platform.runLater(new updaterunnable(Timerx));
                    }
                }catch(Exception e){

                }

            }
        }

        Thread update = new updatethread();
        update.start();
        LineChart.getData().addAll(series);
        LineChart.getData().addAll(series1);
        LineChart.getData().addAll(series2);
        LineChart.getData().addAll(series3);
        LineChart2.getData().addAll(tseries);
        LineChart2.getData().addAll(tseries1);
        LineChart2.getData().addAll(tseries2);
        LineChart2.getData().addAll(tseries3);

        root1.getChildren().addAll(LineChart, LineChart2);
        root1.getChildren().add(dataview);
        primaryStage.setScene(new Scene(root1, 1200, 800));
        primaryStage.show();
    }


    public static void main(String[] args) {
        ServerSide s = new ServerSide();
        Thread Serverthread = new Thread(s);
        Serverthread.start();
        launch(args);



        DatabaseConnection data = new DatabaseConnection();
        data.startConnection();
        Report r = new Report();
        try{
            r.generateReportHour(data.GetDataHour());
        }catch(Exception e){
            System.out.println(e);
        }



    }
}
