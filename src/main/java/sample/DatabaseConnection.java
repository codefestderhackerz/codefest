package sample;
import java.sql.*;
import java.util.ArrayList;



public class DatabaseConnection {
    Connection myConnection;

    public void startConnection(){
        String dbUrl ="jdbc:mysql://localhost:3306/processline";
        String username = "madnisal";
        String password = "password";
        try{
            this.myConnection = DriverManager.getConnection(dbUrl,username,password);
        }catch(Exception e){
            System.out.println(e);
        }

    }

    public ArrayList<ResultSet> GetDataYear(){
        ArrayList<ResultSet> d = new ArrayList<ResultSet>();
        Statement myStatement;
        ResultSet processes;
        try{
            myStatement = this.myConnection.createStatement();
            processes = myStatement.executeQuery("SELECT * from productionLineNames");
            while(processes.next()){
                for (int i =0 ;i<4;i++){
                    myStatement = this.myConnection.createStatement();
                    System.out.println("select Year,SUM(Total) AS Total,productionLine,SUM(Error) AS Error FROM "+processes.getString("name")+" GROUP BY Year,ProductionLine;");
                    ResultSet r = myStatement.executeQuery("select Year,SUM(Total) AS Total,productionLine,SUM(Error) AS Error FROM "+processes.getString("name")+" GROUP BY Year,ProductionLine;");
                    d.add(r);
                }
            }
        }catch(Exception e){
            System.out.println(e);
        }
        System.out.println("d");
        return d;

    }

    public ArrayList<ResultSet> GetDataMonth(){
        ArrayList<ResultSet> d = new ArrayList<ResultSet>();
        Statement myStatement;
        ResultSet processes;
        try{
            myStatement = this.myConnection.createStatement();
            processes = myStatement.executeQuery("SELECT * from productionLineNames");
            while(processes.next()){
                for (int i =0 ;i<4;i++){
                    myStatement = this.myConnection.createStatement();
                    System.out.println("SELECT  * from "+processes.getString("name")+
                            " where productionLine = "+i);
                    ResultSet r = myStatement.executeQuery("select Year,Month,SUM(Error) AS Error,SUM(Total) AS Total,productionLine FROM "+
                            processes.getString("name")+" GROUP BY Year,Month,ProductionLine;");
                    d.add(r);
                }
            }
        }catch(Exception e){
            System.out.println(e);
        }
        System.out.println("d");
        return d;

    }
    public ArrayList<ResultSet> GetDataDay(){
        ArrayList<ResultSet> d = new ArrayList<ResultSet>();
        Statement myStatement;
        ResultSet processes;
        try{
            myStatement = this.myConnection.createStatement();
            processes = myStatement.executeQuery("SELECT * from productionLineNames");
            while(processes.next()){
                for (int i =0 ;i<4;i++){
                    myStatement = this.myConnection.createStatement();
                    System.out.println("SELECT  * from "+processes.getString("name")+
                            " where productionLine = "+i);
                    ResultSet r = myStatement.executeQuery("select Year,Month,Date,SUM(Error) AS Error,SUM(Total) AS Total,productionLine FROM "+processes.getString("name")+
                            " GROUP BY Year,Month,Date,ProductionLine");
                    d.add(r);
                }
            }
        }catch(Exception e){
            System.out.println(e);
        }
        System.out.println("d");
        return d;

    }
    public ArrayList<ResultSet> GetDataHour(){
        ArrayList<ResultSet> d = new ArrayList<ResultSet>();
        Statement myStatement;
        ResultSet processes;
        try{
            myStatement = this.myConnection.createStatement();
            processes = myStatement.executeQuery("SELECT * from productionLineNames");
            while(processes.next()){
                for (int i =0 ;i<4;i++){
                    myStatement = this.myConnection.createStatement();
                    System.out.println("SELECT  * from "+processes.getString("name")+
                            " where productionLine = "+i);
                    ResultSet r = myStatement.executeQuery("SELECT  * from "+processes.getString("name")+
                            " where productionLine = "+i+";");
                    d.add(r);
                }
            }
        }catch(Exception e){
            System.out.println(e);
        }
        System.out.println("d");
        return d;

    }
}
